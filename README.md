Project tile: "EnviroScan: Automated Image Scanning for Instant Plant Identification and Environmental Insights"

Aim:
To develop and implement a comprehensive and user-friendly environment
scanner app tailored to the diverse needs of tourists and nature enthusiasts in Bhutan.
